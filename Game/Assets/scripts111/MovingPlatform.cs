﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

	bool directionLeft;
	[SerializeField]
	float offset;
	bool flag;
	Vector3 startPoint;
	Vector3 endPoint;
	// Use this for initialization
	void Start () {

		directionLeft = true;

		startPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z);
		endPoint = new Vector3(transform.position.x+offset, transform.position.y, transform.position.z);
	}
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag=="Player")
		{
			flag = true;
			collision.collider.transform.SetParent(transform);
		}
	}
	private void OnCollisionExit2D(Collision2D collision)
	{
		if (collision.gameObject.tag=="Player")
		{

			collision.collider.transform.SetParent(null);
		}
	}

	// Update is called once per frame
	void Update () {

		if (directionLeft == true)
		{
			transform.Translate(Vector3.left * Time.deltaTime);
		}
		else
		{
			transform.Translate(Vector3.right * Time.deltaTime);
		}
		if (transform.position.x<=endPoint.x)
		{
			directionLeft = false;         
		}
		if (transform.position.x >= startPoint.x)
		{
			directionLeft = true;
		}




		// if (transform.position == endPoint)
		//{
		//    directionLeft = !directionLeft;
		//}
		//if (!directionLeft)
		//{
		//    transform.Translate(Vector3.right * Time.deltaTime);
		//}
		// if (transform.position == startPoint)
		//{
		//    directionLeft = !directionLeft;
		//}

	}

}
