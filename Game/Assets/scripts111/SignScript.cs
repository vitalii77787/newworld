﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignScript : MonoBehaviour {
	public Image tombimage;
	private bool isaktiveted;
	private bool sethealth;
	public GameObject Healthpacket;
	float [] arr=new float[4];
	// Use this for initialization
	void Start () {
		for (int i = 0; i <arr.Length ; i++) {
			arr [i] = i - 1.5f;
		}
		tombimage.gameObject.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1") && isaktiveted && !sethealth) {
			Sethealth ();
			sethealth = true;
		}
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "Player") 
		{
			isaktiveted = true;
			tombimage.gameObject.SetActive (true);
			//keyImage.color = new Color (1f, 1f, 1f, 1f);
		}
	}

	void OnTriggerExit2D (Collider2D col)
	{
		if (col.tag == "Player") 
		{
			isaktiveted = false;
			tombimage.gameObject.SetActive (false);
		}
	}

	void Sethealth()
	{
		int tmp = Random.Range (0, arr.Length);
		Healthpacket = Instantiate (Healthpacket, new Vector2 (transform.position.x+arr[tmp], transform.position.y), transform.rotation);
			
	}
}