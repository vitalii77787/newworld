﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGameController : MonoBehaviour {
    public static bool isPause;
    public GameObject pauseMenuUI;
	// Use this for initialization
	void Start () {
        isPause = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPause)
            {
                Resume();
            }
            else
            {
                Pause();
            }
            Cursor.visible = true;
        }

    }

   public void Resume()
    {
        FindObjectOfType<AudioManager>().Play("MenuClick");
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPause = false;
        Cursor.visible = false;
    }
    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPause = true;
        Cursor.visible = true;
    }
    public void Menu()
    {
        FindObjectOfType<AudioManager>().Play("MenuClick");
        FindObjectOfType<AudioManager>().Stop("Theme");
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
        // ResetAllStatics();
    }
    public void QuitGame()
    {
        FindObjectOfType<AudioManager>().Play("MenuClick");
        Application.Quit();
    }
    private void ResetAllStatics()
    {
       // ArtefactController.artefactpart = 4;
        //ArtefactController.currentpart = 0;
      //  LaraCroftMoving.isFacingRight = true;
    }
}
