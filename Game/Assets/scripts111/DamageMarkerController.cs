﻿using UnityEngine;
using UnityEngine.UI;

public class DamageMarkerController : MonoBehaviour {
    private Text myText;
    [SerializeField]
    private float moveAmt;
    [SerializeField]
    private float moveSpeed;
    private Vector3[] moveDirs;
    private Vector3 myMoveDir;
    private bool canMove = false;
	// Use this for initialization
	void Start () {
        moveDirs = new Vector3[] { transform.up, (transform.up + transform.right), (transform.up - transform.right) };
        myMoveDir = moveDirs[Random.Range(0, moveDirs.Length)];
	}
	
	// Update is called once per frame
	void Update () {
        if (canMove) transform.position = Vector3.MoveTowards(transform.position, transform.position + myMoveDir, moveAmt * (moveSpeed * Time.deltaTime));
	}

    public void SetTextAndMove(string text, Color textcolor)
    {
        myText = GetComponentInChildren<Text>();
        myText.color = textcolor;
        myText.text = text;
        canMove = true;
    }
}
