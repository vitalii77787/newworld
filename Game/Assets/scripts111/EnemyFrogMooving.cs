﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFrogMooving : MonoBehaviour {
    public float speed = 1;
   protected SpriteRenderer spr;
    protected Rigidbody2D enemypers;
    protected Animator anim;
    public float move = 1;
    public bool isRight;
    bool isSensL, isSensR;
    [SerializeField]
    protected float offset;
    [SerializeField]
    protected float range;
    // Use this for initialization
    protected void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        enemypers = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        offset = 0.25f;
        range = 0.5f;
    }
   protected void FixedUpdate()
    {
        isSensL = Physics2D.Raycast(new Vector2(transform.position.x - offset, transform.position.y), Vector2.down, range);
        isSensR = Physics2D.Raycast(new Vector2(transform.position.x + offset, transform.position.y), Vector2.down, range);
        if (!isSensR)
        {
            move = -speed;
            Flip();
        }
        else if (!isSensL)
        {
            move = speed;
            Flip();
        }
        anim.SetFloat("Speed", Mathf.Abs(move));
        if (enemypers != null)
        {
            enemypers.velocity = new Vector2(move * speed, enemypers.velocity.y);
        }
    }
    // Update is called once per frame
    void Update()
    {

    }

    public void Flip()
    {
        if (move > 0 && !isRight)
        {
            spr.flipX = true;
            isRight = true;
        }
        else if (move < 0 && isRight)
        {
            spr.flipX = false;
            isRight = false;
        }
    }
}
