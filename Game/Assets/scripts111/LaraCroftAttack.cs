﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaraCroftAttack : MonoBehaviour {
    private Animator anim;
    [SerializeField]
    private float attackrange;
    [SerializeField]
    private int attackDamage;
    [SerializeField]
    [Range(0.5f,2f)]
    private float attackDelay;
    [SerializeField]
    private bool isAbleToAttack;
    private LaraCroftMoving move;
    // Use this for initialization
    void Start () {
        SetDefaults();
        anim = GetComponent<Animator>();
        move = GetComponent<LaraCroftMoving>();
    }

	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1") && isAbleToAttack)
        {
          if( GetComponent<LaraCroftMoving>().isGround)
            {
                move.move = 0f;
                FindObjectOfType<AudioManager>().Play("Attack");
                anim.Play("Attack");
                Attackenemy();
                StartCoroutine(AttackDelay());
            }
            return;
        }
    }

    void Attackenemy()
    {
        bool direction = MySingleton.Instance().direction;
        if (direction)
        {
            RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x + 0.1f, transform.position.y), new Vector2(1, 0), attackrange);
            if (hit)
            {
                SetDamage(hit);
            }
        }
        else
        {
            RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x - 0.1f, transform.position.y), new Vector2(-1, 0), attackrange);
            if (hit)
            {
                SetDamage(hit);
            }
        }
    }
    IEnumerator AttackDelay()
    {
        MySingleton.Instance().isattacking = true;
        isAbleToAttack = false;
        yield return new WaitForSeconds(attackDelay);
        isAbleToAttack = true;
        MySingleton.Instance().isattacking = false;
    }
    private void SetDamage(RaycastHit2D hit)
    {
        if (hit.collider.tag == "Enemy")
        {
            hit.collider.GetComponent<EnemyFrogHealth>().TakeDamage(attackDamage);
        }
    }
    private void SetDefaults()
    {
        if (attackrange == 0)
        {
            attackrange = 0.2f;
        }
        if (attackDelay == 0)
        {
            attackDelay = 1f;
        }
        isAbleToAttack = true;
    }
}
