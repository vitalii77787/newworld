﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerAttack : MonoBehaviour {
	public float timeBetweenAttacks = 1f;
	public int attackDamage = 20;   
	Animator anim;  
	CharacterControllerScript control;
	EnemyHealth enemyhealth;
	EnemyMove enemymove;
	bool enemyInRange;                         // Whether player is within the trigger collider and can be attacked.
	float timer;     
	bool isRight;
	public Vector2 pointpos;
	float attackrange;
	PlayerHealth ph;
	public AudioClip attack;
	// Use this for initialization
	void Awake ()
	{
		
		attackrange = 0.4f;
		control = GetComponent<CharacterControllerScript> ();
		anim = GetComponent <Animator> ();
		ph = GetComponent<PlayerHealth> ();
	}
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		isRight = GetComponent<CharacterControllerScript> ().isFacingRight;
		if (Input.GetButtonDown ("Fire1") && !ph.isDead) {
			GetComponent<AudioSource> ().clip = attack;
			GetComponent<AudioSource> ().Play ();
			print ("first");
			Attackenemy (isRight);
		}
	}
	void Attackenemy(bool param)
	{
		if (param) {
			RaycastHit2D hit = Physics2D.Raycast (new Vector2 (transform.position.x + 0.3f, transform.position.y), new Vector2(1,0), attackrange);
			if (hit) {
				print (hit.collider.name);
				if (hit.collider.tag == "Enemy") {
					hit.collider.GetComponent<EnemyHealth> ().TakeDamage (attackDamage);
				}
			}
		} 
		else 
		{
			RaycastHit2D hit = Physics2D.Raycast (new Vector2(transform.position.x-0.3f,transform.position.y), new Vector2(-1,0), attackrange);
			if (hit) {
				print (hit.collider.name);
				if (hit.collider.tag == "Enemy")
				{
					hit.collider.GetComponent<EnemyHealth> ().TakeDamage (attackDamage);

				}
					}
		}
	}
}
