﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gold : MonoBehaviour {
    private float randomedTime;
    [SerializeField]
    protected GameObject clockprefab;
    void Awake()
	{
        randomedTime = Random.Range(10, 16);
	}
		

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.collider.tag == "Player") 
		{
            FindObjectOfType<AudioManager>().Play("Pickup1");
            GameManager.Instance().AddTime(randomedTime);
            GameObject newMarker = Instantiate(clockprefab, new Vector3(transform.position.x, transform.position.y + 0.25f, transform.position.z), Quaternion.identity);
            newMarker.GetComponent<DamageMarkerController>().SetTextAndMove("+"+randomedTime.ToString()+"sec", Color.yellow);
            Destroy (gameObject);
		}
	}
}
