﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCockHealth : EnemyFrogHealth {

  


    // Update is called once per frame
 
  
   protected override void Death()
    {
        isDead = true;
        anim.Play("Death");
        DisableAttack();
        DisableMovement();
        CreateGold();
        Destroy(enemycollider, 0.4f);
        Destroy(gameObject, 1f);
    }

}
