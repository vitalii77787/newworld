﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCockAttack : EnemyFrogAttack {
   
   
    protected override void Attack()
    {
        anim.Play("Attack");
        timer = 0f;
        // If the player has health to lose...
        if (playerHealth.currentHealth > 0)
        {
            // ... damage the player.
            playerHealth.TakeDamage(attackDamage);
        }
    }
}
