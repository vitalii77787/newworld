﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemycanvasscr : MonoBehaviour {
	public GameObject enemy;
	public Image img;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (enemy.GetComponent<Transform> ().localScale.x < 0) {
			img.fillOrigin = 1;
		} 
		else
		{
			img.fillOrigin = 0;
		}
	}
}
