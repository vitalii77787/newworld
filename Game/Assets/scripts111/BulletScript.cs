﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
    [SerializeField]
	private int attackDamage;
    [SerializeField]
    public bool isRight;
    [Range(0,5)]
    [SerializeField]
    private float speed;
	private float Timer;
    [SerializeField]
    private Vector3 rotation;
	// Use this for initialization
	void Start () {
		Timer = 0;
        if(speed==0)
        {
            speed = 1f;
        }
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(rotation, Space.World);
        Timer += Time.deltaTime;
		if (isRight) 
		{
			transform.Translate (Vector3.right * Time.deltaTime * speed,Space.World);
		} 
		else {
			transform.Translate (Vector3.left * Time.deltaTime * speed,Space.World);
		}
		if (Timer>3f) {
			Destroy (gameObject); 
		}
	}
    public void SetRotation(bool direction)
    {
        isRight = direction;
        if(direction)
        {
            rotation = new Vector3(0f, 0f, -3f);
        }
        else
        {
            rotation = new Vector3(0f, 0f, 3f);
        }
    }
	void OnTriggerEnter2D  (Collider2D сollision)
	{
		if (сollision.gameObject.tag== "Enemy") {
            сollision.gameObject.GetComponent<EnemyFrogHealth>().TakeDamage(attackDamage);
            //сollision.gameObject.GetComponent<EnemyHealth> ().TakeDamage (attackDamage);
            Destroy (gameObject); 
		}

	}
	//todo:111
}
