﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyScript : MonoBehaviour {
	public Image tombimage;
	private bool isaktiveted;
	private bool setKey;
	public GameObject Key;
	// Use this for initialization
	void Start () {
		tombimage.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1") && isaktiveted && !setKey) {
			SetKey ();
			setKey = true;
		}
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "Player") 
		{
			isaktiveted = true;
			tombimage.gameObject.SetActive (true);
			//keyImage.color = new Color (1f, 1f, 1f, 1f);
		}
	}

	void OnTriggerExit2D (Collider2D col)
	{
		if (col.tag == "Player") 
		{
			isaktiveted = false;
			tombimage.gameObject.SetActive (false);
		}
	}

	void SetKey()
	{
		Key = Instantiate (Key, new Vector2 (transform.position.x-2.5f, transform.position.y), transform.rotation);

	}
}
