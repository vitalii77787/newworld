﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArtefactController : MonoBehaviour {
    [SerializeField]
    private GameObject healpacksHolder;
    [SerializeField]
    private GameObject healpackPrefab;
    [SerializeField]
    private int artefactcount;
    [SerializeField]
    private Component[] ValidPositions;
    [SerializeField]
    private List<Component> Usedpositions;
    [SerializeField]
    private GameObject[] HealPositions;
    // Use this for initialization
    void Start () {
        //artefactcount = 4;
        artefactcount = 3;
        Usedpositions = new List<Component>();
        SetPositions();
        SetHealPacks();
	}
	
	// Update is called once per frame
	
    private void SetPositions()
    {
        while(artefactcount>=0)
        {
            int currentposition = Random.Range(0, ValidPositions.Length);
            Component current = ValidPositions[currentposition];
            if(Usedpositions.Contains(current))
            {
               continue;
            }
            else
            {
                Usedpositions.Add(current);
                current.GetComponent<ArtefactPlace>().InitArtefact(artefactcount, true);
                //int position = artefactcount - 1;
                artefactcount--;
                //current.GetComponent<ArtefactPlace>().InitArtefact(position, true);
            }
        }
    }
    private void SetHealPacks()
    {
        if(healpackPrefab==null || HealPositions.Length==0 || healpacksHolder==null)
        {
            return;
        }
        else
        {
            foreach (var item in HealPositions)
            {
                var healpackcuurent = Instantiate(healpackPrefab, new Vector3(item.transform.position.x, item.transform.position.y, item.transform.position.z), Quaternion.identity, healpacksHolder.transform);
            }
        }
    }
}
