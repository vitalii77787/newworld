﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MainMenuController : MonoBehaviour {
    public AudioMixer audioMixer;
    public AudioMixer soundMixer;
    public Dropdown resolutionDropdown;
    Resolution[] resolutions;
    // Use this for initialization
    void Start() {
        resolutions = Screen.resolutions;
        resolutions = RemoveDuplicates(resolutions);
        Debug.Log("resolution length = " + resolutions.Length);
        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();
        int currentresolution = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentresolution = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentresolution;
        resolutionDropdown.RefreshShownValue();
    }

    // Update is called once per frame
    void Update() {

    }
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
    public void StartGame()
    {

        SceneManager.LoadScene("FirstLevel");
    }
    public void StartMusic()
    {
        FindObjectOfType<AudioManager>().Play("Theme");
    }
    public void QuitGame()
    {
        FindObjectOfType<AudioManager>().Play("MenuClick");
        Application.Quit();
    }
    public void SetSoundVolume(float value)
    {
        soundMixer.SetFloat("SoundVolume", ConvertFloat(value));
    }
    public void SetVolume(float value)
    {
        audioMixer.SetFloat("volume", ConvertFloat(value));
    }
    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }
    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }
    private float ConvertFloat(float value)
    {
        float converted = 0;
        converted=value * 100 - 80;
        return converted;
    }
    public static Resolution[] RemoveDuplicates(Resolution[] s)
    {
        HashSet<Resolution> set = new HashSet<Resolution>(s);
        Resolution[] result = new Resolution[set.Count];
        set.CopyTo(result);
        return result;
    }
    public void SoundClick()
    {
        FindObjectOfType<AudioManager>().Play("MenuClick");
    }
}
