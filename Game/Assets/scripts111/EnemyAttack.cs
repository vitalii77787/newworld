﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

	public float timeBetweenAttacks = 1f;     // The time in seconds between each attack.
	public int attackDamage = 10;               // The amount of health taken away per attack.

	EnemyMove move;
	Animator anim;                              // Reference to the animator component.
	GameObject player;                          // Reference to the player GameObject.
	PlayerHealth playerHealth;                  // Reference to the player's health.
//	EnemyHealth enemyHealth;                    // Reference to this enemy's health.
	bool playerInRange;                         // Whether player is within the trigger collider and can be attacked.
	float timer;                                // Timer for counting up to the next attack.
	EnemyMove enemymove;
	SpriteRenderer spr;
	void Awake ()
	{
		spr = GetComponent<SpriteRenderer> ();
		// Setting up the references.
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		//enemyHealth = GetComponent<EnemyHealth>();
		anim = GetComponent <Animator> ();
		move=gameObject.GetComponent<EnemyMove>();
		enemymove = GetComponent<EnemyMove> ();
	}


	void OnCollisionEnter2D (Collision2D other)
	{
		print ("+++");
		// If the entering collider is the player...
		if (other.gameObject == player) {
			print ("Attackplayer!");
			// ... the player is in range.
			playerInRange = true;
			if (other.contacts [0].normal.x == 1 && (enemymove.isRight==true)) {
				spr.flipX = !spr.flipX;
				enemymove.Flip ();
				enemymove.move = -1f * enemymove.speed;
			} else if (other.contacts [0].normal.x == -1 && (enemymove.isRight==false)) {
				spr.flipX = !spr.flipX;
				enemymove.Flip ();
				enemymove.move = 1f * enemymove.speed;
			}
		}
		}



	void OnCollisionExit2D (Collision2D other)
	{
		// If the exiting collider is the player...
		if(other.gameObject == player)
		{
			// ... the player is no longer in range.
			playerInRange = false;
		}
	}


	void Update ()
	{
		// Add the time since Update was last called to the timer.
		timer += Time.deltaTime;

		// If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
		if (timer >= timeBetweenAttacks && playerInRange && (!(playerHealth.isDead)) ) {
			// ... attack.
			Attack ();
			move.enabled = false;
			anim.SetBool ("canwalk", false);
		} else if (!playerInRange) 
		{
			move.enabled = true;
			anim.SetBool ("canwalk", true);
		}

		// If the player has zero or less health...
		if(playerHealth.currentHealth <= 0)
		{
			// ... tell the animator the player is dead.
			//anim.SetTrigger ("PlayerDead");
		}
	}


	void Attack ()
	{
		int temp = Random.Range (1, 4);
		if(enemymove.persobj)
		{
			switch (temp) 
			{
			case 1:
				anim.Play ("Attack");
				break;
			case 2:
				anim.Play ("Attack2");
				break;
			case 3:
				anim.Play ("Attack3");
				break;
			}
		}
		else
		{
		anim.Play("Attack");
		}
		// Reset the timer.
		timer = 0f;
		// If the player has health to lose...
		if(playerHealth.currentHealth > 0)
		{
			// ... damage the player.
			playerHealth.TakeDamage (attackDamage);
		}
	}
}
