﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject TimePanel;
    private Component UITimerText;
    private static GameManager mInstance;
    public static GameManager Instance() { return mInstance; }
    void Awake()
    {
        if (!mInstance) mInstance = this;
    }
    private float maxTime;
    private string timerString;
    private const float MaxTime = 90;
    [SerializeField]
    private GameObject artefact;
    [SerializeField]
    private Component[] artefactparts;
    [SerializeField]
    private Text artefactCountText;
    // Use this for initialization
    void Start()
    {
        timerString = "Time left: ";
        UITimerText = TimePanel.GetComponentInChildren(typeof(Text), true);
        (UITimerText as Text).text = timerString + MaxTime.ToString();
        maxTime = MaxTime;
        artefactCountText = GameObject.FindGameObjectWithTag("ArtefactCount").GetComponent<Text>();
        artefact = GameObject.FindGameObjectWithTag("Artefact");
        artefactparts = artefact.GetComponentsInChildren(typeof(Image), true);
        StartCoroutine("MyEvent");
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetArtefact(int partnumber)
    {
        (artefactparts[partnumber] as Image).color = new Color((artefactparts[partnumber] as Image).color.r, (artefactparts[partnumber] as Image).color.g, (artefactparts[partnumber] as Image).color.b, 100f);
        UpdateText();
    }
    private void UpdateText()
    {
        UpdateCounter();
        artefactCountText.text = MySingleton.Instance().currentartefactParts.ToString() /*ArtefactController.currentpart.ToString()*/ + " / " + MySingleton.Instance().artefactParts.ToString();/* ArtefactController.artefactpart.ToString()*/;
    }
    private void UpdateCounter()
    {
        MySingleton.Instance().currentartefactParts++;
        CheckWinCondition();

    }
    private void CheckWinCondition()
    {
        if (MySingleton.Instance().currentartefactParts < MySingleton.Instance().artefactParts)
        {
            FindObjectOfType<AudioManager>().Play("FindArtefact");
        }
        else
        {
            FindObjectOfType<AudioManager>().Play("FindAll");
        }

    }


    private IEnumerator MyEvent()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.0f); // wait half a second
            maxTime--;
            UpdateUiTimer();
            if (maxTime <= 0.0f)
            {
                LoseCondition();
                break;
            }
        }
    }

    private void LoseCondition()
    {
        throw new NotImplementedException();
    }

    private void UpdateUiTimer()
    {

        (UITimerText as Text).text = timerString + maxTime.ToString();
        ChangeUiTimerColor();
    }

    private void ChangeUiTimerColor()
    {
        if (maxTime < MaxTime * 0.33)
        {
            (UITimerText as Text).color = Color.red;
        }
        else if (maxTime < MaxTime * 0.66)
        {
            (UITimerText as Text).color = Color.yellow;
        }
    }
    public void AddTime(float add)
    {
        if((maxTime+add)<=MaxTime)
        {
            maxTime += add;
        }
        else
        {
            maxTime = MaxTime;
        }
        UpdateUiTimer();
    }
}
