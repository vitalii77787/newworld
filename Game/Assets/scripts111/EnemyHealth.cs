﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {
    [SerializeField]
    private GameObject hitMarkerPrefab;
    [SerializeField]
    private float markerKillTime;
	public GameObject gold;
	public Image healthBar;
	public float maxHealth = 100;                            // The amount of health the player starts the game with.
	public float currentHealth;  
	EnemyMove enemymove;
	EnemyAttack attack;
	//public Scrollbar healthbar;  
	Animator anim;   
	bool isDead;  
	GameObject player;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		attack = GetComponent<EnemyAttack> ();
		currentHealth = maxHealth;
		anim = GetComponent <Animator> ();
		enemymove = GetComponent <EnemyMove> ();
		UpdateHealthBar ();

	}
	
	// Update is called once per frame
	void Update () {
	}
	public void TakeDamage (int amount)
	{
		WithdrawHealth (amount);
		UpdateHealthBar ();
		// Play the hurt sound effect.
		//playerAudio.Play ();
	}
	void Death ()
	{
		// Set the death flag so this function won't be called again.
		isDead = true;
		// Turn off any remaining shooting effects.
		//playerShooting.DisableEffects ();
		// Tell the animator that the player is dead.
		anim.Play("Dead");
		// Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
		//playerAudio.clip = deathClip;
		//playerAudio.Play ();
		// Turn off the movement and shooting scripts.
		DisableAttack();
		DisableMovement ();
		CreateGold ();
		Destroy (gameObject, 0.6f);
		//playerShooting.enabled = false;
	}       
	private void UpdateHealthBar()
	{
		healthBar.fillAmount = GetHealthValue();
	}

	private float GetHealthValue()
	{
		return (float)currentHealth / (float)maxHealth;
	}

	private void WithdrawHealth(int amount)
	{
        CreateHitMarker(amount);
		if (currentHealth - amount <= 0 && !isDead) {
			currentHealth = 0;
			// If the player has lost all it's health and the death flag hasn't been set yet...
			Death ();
		} else {
			currentHealth -= amount;
		}
	}

	private void CreateGold()
	{
		if (enemymove.persobj) {
			gold = Instantiate (gold, new Vector2 (transform.position.x, transform.position.y + 0.4f), player.transform.rotation);
			gold.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range (-150f, 150f), 800));
		} 
		else {
			gold = Instantiate (gold, new Vector2 (transform.position.x, transform.position.y + 0.3f), transform.rotation);
			gold.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range (-150f, 150f), 800));
		}
	}
	private void DisableMovement()
	{
		enemymove.enabled = false;
	}
	private void DisableAttack()
	{
		attack.enabled=false;
	}
    private void CreateHitMarker(float damageamount)
    {
        Color markercolor = Color.green;
        GameObject newMarker = Instantiate(hitMarkerPrefab, new Vector3(transform.position.x, transform.position.y+0.5f, transform.position.z), Quaternion.identity);
        newMarker.GetComponent<DamageMarkerController>().SetTextAndMove(damageamount.ToString(), markercolor);
        Destroy(newMarker.gameObject, markerKillTime);
    }
}
