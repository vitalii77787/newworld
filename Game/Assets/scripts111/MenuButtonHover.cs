﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButtonHover : MonoBehaviour {
    [SerializeField]
   private Component buttonText;
	// Use this for initialization
	void Start () {
        buttonText=GetComponentInChildren(typeof (Text), true);
        buttonText.gameObject.SetActive(false);

    }
    void OnMouseEnter()
    {
        buttonText.gameObject.SetActive(true);
    }
    void OnMouseExit()
    {
        buttonText.gameObject.SetActive(false);
    }
}
