﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightMovingPlatform : MonoBehaviour
{
    [Range(-10, 10)]
    [SerializeField]
    float offsetX;
    [Range(-10, 10)]
    [SerializeField]
    float offsetY;
    private Vector3 startPoint;
    private Vector3 endPointX;
    private Vector3 endPointY;
    private bool directionRight;
    // Use this for initialization
    void Start()
    {
        directionRight = true;
        startPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        endPointX = new Vector3(transform.position.x + offsetX, transform.position.y, transform.position.z);
        endPointY = new Vector3(transform.position.x, transform.position.y + offsetY, transform.position.z);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.collider.transform.SetParent(transform);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.collider.transform.SetParent(null);
        }
    }

    // Update is called once per frame
    void Update()
    {
        MovePlatform(offsetX, offsetY);
    }
    void MovePlatform(float offsetX, float offsetY)
    {
        if (offsetX > 0 && offsetY == 0)
        {
            if (directionRight == true)
            {
                transform.Translate(Vector3.right * Time.deltaTime);
                if (transform.position.x >= endPointX.x)
                {
                    directionRight = false;
                }
            }
            else
            {
                transform.Translate(Vector3.left * Time.deltaTime);
                if (transform.position.x <= startPoint.x)
                {
                    directionRight = true;
                }
            }
        }
        else if (offsetX < 0 && offsetY == 0)
        {
            if (directionRight == false)
            {
                transform.Translate(Vector3.right * Time.deltaTime);
                if (transform.position.x >= startPoint.x)
                {
                    directionRight = true;
                }
            }
            else
            {
                transform.Translate(Vector3.left * Time.deltaTime);
                if (transform.position.x <= endPointX.x)
                {
                    directionRight = false;
                }
            }
        }
        else if (offsetY > 0 && offsetX == 0)
        {
            if (directionRight == true)
            {
                transform.Translate(Vector3.up * Time.deltaTime);
                if (transform.position.y >= endPointY.y)
                {
                    directionRight = false;
                }
            }
            else
            {
                transform.Translate(Vector3.down * Time.deltaTime);
                if (transform.position.y <= startPoint.y)
                {
                    directionRight = true;
                }
            }
        }
        else if (offsetY < 0 && offsetX == 0)
        {
            if (directionRight == false)
            {
                transform.Translate(Vector3.up * Time.deltaTime);
                if (transform.position.y >= startPoint.y)
                {
                    directionRight = true;
                }
            }
            else
            {
                transform.Translate(Vector3.down * Time.deltaTime);
                if (transform.position.y <= endPointY.y)
                {
                    directionRight = false;
                }
            }
        }
    }
}