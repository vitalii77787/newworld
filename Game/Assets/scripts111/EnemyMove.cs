﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {
	public float speed = 1;
	public LayerMask layerGround;
	public Transform persobj;
	Transform player;
	SpriteRenderer spr;
	Rigidbody2D enemypers;
	Animator anim;
	public float move=1;
	public bool isRight;
	bool isGround=false;
	bool isSensL, isSensR;
	float timer=0;
	Vector2 leftpos;
	Vector2 rightpos;


	// Use this for initialization
	void Start () {
		spr = GetComponent<SpriteRenderer> ();
		enemypers = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
	}


	void FixedUpdate()
	{
		isGround=Physics2D.Raycast (transform.position, Vector2.down, 0.5f,layerGround);
		anim.SetBool("isGround", isGround);
		anim.SetFloat("ySpeed", enemypers.velocity.y);
		leftpos = new Vector2 (transform.position.x - 0.25f, transform.position.y);
		rightpos = new Vector2 (transform.position.x + 0.25f, transform.position.y);
		isSensL=Physics2D.Raycast (leftpos, Vector2.down, 0.5f,layerGround);
		isSensR=Physics2D.Raycast (rightpos, Vector2.down, 0.5f,layerGround);
		Debug.DrawRay (leftpos, Vector2.down);
		Debug.DrawRay (rightpos, Vector2.down);
		//print (isSensL+"   "+isSensR);
		if (isRight && !isSensR) 
		{
			move = -1f * speed;
		} 
		else if (!isRight && !isSensL) 
		{
			move = 1f * speed;
		}
		anim.SetFloat ("Speed", Mathf.Abs (move));
		enemypers.velocity = new Vector2 (move * speed, enemypers.velocity.y);
		Flip ();
	}
	// Update is called once per frame
	void Update () {
		
	}

	public void Flip()
	{
		
	
		if (move > 0 && !isRight) {
			if (persobj) {
				Vector3 scale = transform.localScale;
				scale.x *= -1;
				transform.localScale = scale;
			}
			else{
				spr.flipX = false;
			}
				isRight = true;
			} else if (move < 0 && isRight) {

			if (persobj) {
				Vector3 scale = transform.localScale;
				scale.x *= -1;
				transform.localScale = scale;
			}
			else{
				spr.flipX = true;
			}
				isRight = false;
			}
		
	}
}
