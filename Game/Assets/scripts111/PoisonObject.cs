﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonObject : MonoBehaviour {
    [SerializeField]
    private bool flag;
    [SerializeField]
    private bool isHeroinRange;
    PlayerHealth playerhealth;
    GameObject player;
    // Use this for initialization
    void Start() {
        player = GameObject.FindGameObjectWithTag("Player");
        isHeroinRange = false;
        flag = false;
        StartCoroutine(Example());
        playerhealth = player.GetComponent<PlayerHealth>();
    }

    IEnumerator Example()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            flag = !flag;
            if (flag && isHeroinRange)
            {
                playerhealth.TakeDamage(10);
            }
        }
    }
 
    // Update is called once per frame
    void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D сollision)
    {
        if (сollision.tag == "Player")
        {
            isHeroinRange = true;
        }
    }
    void OnTriggerExit2D(Collider2D сollision)
    {
        if (сollision.tag == "Player")
        {
            isHeroinRange = false;
        }
    }
}
