﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyActivated : MonoBehaviour {
	Image Keyimage;
	GameObject player;
	bool istoken;
	public GameObject gamehelper;
	public GameObject girlprefab;
	public AudioClip keymusic;
	// Use this for initialization
	void Start () {
		Keyimage = GameObject.FindGameObjectWithTag ("keyImage").GetComponent<Image> ();
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject==player && !istoken) 
		{
			istoken = true;
			GetComponent<AudioSource> ().clip = keymusic;
			GetComponent<AudioSource> ().Play ();
			Instantiate (girlprefab, gamehelper.transform.position, Quaternion.identity);
			//audioclip.Play ();
			GetComponent<SpriteRenderer> ().enabled=false;
			Keyimage.color = new Color (1f, 1f, 1f, 1f);
			Destroy (gameObject,3f);
		}
	}
}
