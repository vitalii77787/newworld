﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace AssemblyCSharp
{
	public abstract class BaseHealth: MonoBehaviour
	{
		protected bool damaged {get; private set;}
		public static readonly int maxHealth = 100;                            // The amount of health the player starts the game with.
		public int currentHealth {get; private set;}                                   // The current health the player has.
		public bool isDead {get; private set;}                                                // Whether the player is dead.
		public Slider healthSlider;                                 // Reference to the UI's health bar.

		private Animator anim;                                              // Reference to the Animator component.

		public BaseHealth ()
		{
		}

		public void Awake ()
		{
			// Setting up the references.
			anim = GetComponent <Animator> ();
			//playerAudio = GetComponent <AudioSource> ();
			//playerShooting = GetComponentInChildren <PlayerShooting> ();
			// Set the initial health of the player.
			currentHealth = maxHealth;
		}

		public void TakeDamage(int amount) 
		{
			damaged = true;
			// Reduce the current health by the damage amount.
			WithdrawHealth(amount);
			// Set the health bar's value to the current health.

			UpdateHealthSlider ();
		}

		public void TakeHealth(int amount)
		{
			AddHealth (amount);

			UpdateHealthSlider ();
		}

		protected void AddHealth(int amount)
		{
			currentHealth = currentHealth + amount >= maxHealth ? maxHealth : currentHealth + amount;
		}

		protected void WithdrawHealth(int amount)
		{
			if (currentHealth - amount <= 0) {
				currentHealth = 0;
				// If the player has lost all it's health and the death flag hasn't been set yet...
				Die ();
			} else {
				currentHealth -= amount;
			}
		}

		protected void Die() 
		{
			// Set the death flag so this function won't be called again.
			isDead = true;
			// Turn off any remaining shooting effects.
			//playerShooting.DisableEffects ();
			anim.Play("Death");
			// Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
			//playerAudio.clip = deathClip;
			//playerAudio.Play ();

			// Turn off the movement and shooting scripts.
		DisableMovement();
			//playerShooting.enabled = false;
		}

		protected virtual void DisableMovement()
		{
		}

		protected void UpdateHealthSlider()
		{
			healthSlider.value = GetHealthValue();
		}

		protected float GetHealthValue()
		{
			return (float)currentHealth / (float)maxHealth * 100;
		}
	}
}

