﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth :MonoBehaviour 
{
	public static readonly int maxHealth = 100;                            // The amount of health the player starts the game with.
	public static readonly float flashSpeed = 5f;                               // The speed the damageImage will fade at.
	public static readonly Color flashColour = new Color(1f, 0f, 0f, 0.1f);     // The colour the damageImage is set to, to flash.

	public int currentHealth {get; private set;}                                   // The current health the player has.	
	public bool isDead {get; private set;}                                                // Whether the player is dead.

	public Image damageImage;                                   // Reference to an image to flash on the screen on being hurt.
	//public AudioClip deathClip;                                 // The audio clip to play when the player dies.
	public Slider healthSlider;                                 // Reference to the UI's health bar.
	private Animator anim;                                              // Reference to the Animator component.
	//AudioSource playerAudio;                                    // Reference to the AudioSource component.
	private LaraCroftMoving playerMovement;                              // Reference to the player's movement.
    private LaraCroftAttack playerAttack;
    private WeaponPanel weapons;
    private Collider2D playercollider;
    private Rigidbody2D playerrigidbody;
	//PlayerShooting playerShooting;                              // Reference to the PlayerShooting script.

	private bool damaged {get; set;}  // True when the player gets damaged.    
	public AudioClip damageaudio;
	   

	public void Awake ()
	{
		// Setting up the references.
		anim = GetComponent <Animator> ();
		playerMovement = GetComponent <LaraCroftMoving> ();
        playerAttack = GetComponent<LaraCroftAttack>();
        weapons = GetComponent<WeaponPanel>();
        playercollider = GetComponent<Collider2D>();
        playerrigidbody = GetComponent<Rigidbody2D>();
		//playerAudio = GetComponent <AudioSource> ();
		//playerShooting = GetComponentInChildren <PlayerShooting> ();
		// Set the initial health of the player.
		currentHealth = maxHealth;
	}

	public void Update ()
	{
        if(Input.GetKeyDown(KeyCode.O))
        {
            TakeDamage(100);
        }

        damageImage.color = damaged ? flashColour : GetUndamagedColor ();
		damaged = false;
	}

	public void TakeDamage (int amount)
	{
		// Set the damaged flag so the screen will flash.
		damaged = true;
		// Reduce the current health by the damage amount.
		WithdrawHealth(amount);
		// Set the health bar's value to the current health.
		
		// Play the hurt sound effect.
		GetComponent<AudioSource>().clip=damageaudio;
		GetComponent<AudioSource> ().Play ();
	}

	public void TakeHealth(int amount)
	{
		AddHealth (amount);
		UpdateHealthSlider ();
	}

	private void AddHealth(int amount)
	{
		currentHealth = currentHealth + amount >= maxHealth ? maxHealth : currentHealth + amount;
	}

	private void WithdrawHealth(int amount)
	{
		if (currentHealth - amount <= 0) {
			currentHealth = 0;
            UpdateHealthSlider();
            // If the player has lost all it's health and the death flag hasn't been set yet...
            Die ();
		} else {
			currentHealth -= amount;
            UpdateHealthSlider();
        }

	}

	private void Die ()
	{
		// Set the death flag so this function won't be called again.
		isDead = true;
		// Turn off any remaining shooting effects.
		//playerShooting.DisableEffects ();
		anim.Play("Death");
		// Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
		//playerAudio.clip = deathClip;
		//playerAudio.Play ();

		// Turn off the movement and shooting scripts.
		DisableMovementAndAttack();
		//playerShooting.enabled = false;
	} 

	private void UpdateHealthSlider()
	{
		healthSlider.value = GetHealthValue();
	}

	private float GetHealthValue()
	{
		return (float)currentHealth / (float)maxHealth * 100;
	}

	private Color GetUndamagedColor()
	{
		
		return Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
	}

	private void DisableMovementAndAttack()
	{
		playerMovement.enabled = false;
        playerAttack.enabled = false;
        weapons.enabled = false;
	}
}
