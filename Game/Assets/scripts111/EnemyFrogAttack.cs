﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFrogAttack : MonoBehaviour {

    public float timeBetweenAttacks = 1f;     // The time in seconds between each attack.
    public int attackDamage = 10;               // The amount of health taken away per attack.
    [SerializeField]
    protected EnemyFrogMooving move;
    [SerializeField]
    protected Animator anim;                              // Reference to the animator component.
    [SerializeField]
    protected GameObject player;                          // Reference to the player GameObject.
    [SerializeField]
    protected PlayerHealth playerHealth;                  // Reference to the player's health.
                                                          //	EnemyHealth enemyHealth;                    // Reference to this enemy's health.
    [SerializeField]
    protected bool playerInRange;                         // Whether player is within the trigger collider and can be attacked.
    [SerializeField]
    protected float timer;                                // Timer for counting up to the next attack.
    [SerializeField]
    protected EnemyFrogMooving enemymove;
    [SerializeField]
    protected SpriteRenderer spr;
   protected void Awake()
    {
        spr = GetComponent<SpriteRenderer>();
        // Setting up the references.
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        //enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent<Animator>();
        move = gameObject.GetComponent<EnemyFrogMooving>();
        enemymove = GetComponent<EnemyFrogMooving>();
    }


   protected virtual void OnCollisionEnter2D(Collision2D other)
    {
        print("+++");
        // If the entering collider is the player...
        if (other.gameObject == player)
        {
            move.enabled = false;
            print("Attackplayer!");
            // ... the player is in range.
            playerInRange = true;
            if (other.contacts[0].normal.x == 1 && (enemymove.isRight == true))
            {
                spr.flipX = !spr.flipX;
                enemymove.Flip();
                enemymove.move = -1f * enemymove.speed;
            }
            else if (other.contacts[0].normal.x == -1 && (enemymove.isRight == false))
            {
                spr.flipX = !spr.flipX;
                enemymove.Flip();
                enemymove.move = 1f * enemymove.speed;
            }
        }
    }



    protected virtual void OnCollisionExit2D(Collision2D other)
    {
        // If the exiting collider is the player...
        if (other.gameObject == player)
        {
            // ... the player is no longer in range.
            playerInRange = false;
            move.enabled = true;
        }
    }


   protected void Update()
    {
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if (timer >= timeBetweenAttacks && playerInRange && (!(playerHealth.isDead)))
        {
            // ... attack.
            Attack();
            anim.SetBool("canwalk", false);
        }
        else if (!playerInRange)
        {
            anim.SetBool("canwalk", true);
        }

        // If the player has zero or less health...
       
    }


   protected virtual  void Attack()
    {
        anim.Play("FrogAttack");
        
        // Reset the timer.
        timer = 0f;
        // If the player has health to lose...
        if (playerHealth.currentHealth > 0)
        {
              // ... damage the player.
            playerHealth.TakeDamage(attackDamage);
        }
    }
}
