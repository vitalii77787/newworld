﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tombstonescript : MonoBehaviour {
	public Image tombimage;
	private bool isaktiveted;
	private bool setgold;
	public GameObject gold;
	// Use this for initialization
	void Start () {
		tombimage.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1") && isaktiveted && !setgold) {
			Setgold ();
			setgold = true;
		}
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "Player") 
		{
			isaktiveted = true;
			tombimage.gameObject.SetActive (true);
			//keyImage.color = new Color (1f, 1f, 1f, 1f);
		}
	}

	void OnTriggerExit2D (Collider2D col)
	{
		if (col.tag == "Player") 
		{
			isaktiveted = false;
			tombimage.gameObject.SetActive (false);
		}
	}

	void Setgold()
	{
		int tmp = Random.Range (3, 6);
		for (int i = 0; i < tmp; i++)
		{
			gold = Instantiate (gold, new Vector2 (transform.position.x, transform.position.y + 0.5f), transform.rotation);
			gold.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range (-150f, 150f), 800));
		}
	}
}

