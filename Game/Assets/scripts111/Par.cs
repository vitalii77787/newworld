﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Par : MonoBehaviour {
	public Transform[] pmas;
	public float run;
	public float jump;

	void Update()
	{		
		if (Mathf.Abs(run) > 0.1f)
		{
			pmas [0].localPosition -= new Vector3(0.02f * run, 0, 0);
			pmas [1].localPosition -= new Vector3(0.02f * run, 0, 0);
			pmas [2].localPosition -= new Vector3(0.04f * run, 0, 0);
			pmas [3].localPosition -= new Vector3(0.04f * run, 0, 0);

			for (int i = 0; i < 4; ++i)
			{
				if (pmas [i].localPosition.x < -7.45f)
				{
					pmas [i].localPosition += new Vector3(14.9f, 0, 0);
				}
				if (pmas [i].localPosition.x > 7.45f)
				{
					pmas [i].localPosition -= new Vector3(14.9f, 0, 0);
				}
			}
		}

		if (Mathf.Abs(jump) > 0.01f)
		{
			pmas [0].localPosition -= new Vector3(0, 0.4f * Time.deltaTime * jump, 0);
			pmas [1].localPosition -= new Vector3(0, 0.4f * Time.deltaTime * jump, 0);
			pmas [2].localPosition -= new Vector3(0, 0.8f * Time.deltaTime * jump, 0);
			pmas [3].localPosition -= new Vector3(0, 0.8f * Time.deltaTime * jump, 0);

		}
	}
}
