﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponPanel : MonoBehaviour {
    [SerializeField]
    private GameObject [] bullet; // bullet objects - red or yellow or blue prefab of bullet
    [SerializeField]
	private GameObject bulletcontainer; // empty gameobject to collect all shooted bullets 
   //public List <int> bulletscounter = new List<int> (); //the counters of bullets count int list
   //public int selectedweapon = 0; // current weapon
    [SerializeField]
    private int bulletscounter;
    [SerializeField]
    private GameObject weaponPanel; // panel with weapon icons inside goldpanel
    [SerializeField]
    private Component[] weapons; // array of weapon icons
    [SerializeField]
    private List<Image> weaponlist=new List<Image>(); // list of weapon icons
    [SerializeField]
    /*private Text[] bulletstext=new Text[1];*/  //array of weapon Texts
    private Text bulletstext;
	bool isRight; // bolean value of current hero direction

	// Use this for initialization
	void Start () {
        
        bulletcontainer = GameObject.FindGameObjectWithTag ("bulletcontainer");
		weaponPanel = GameObject.FindGameObjectWithTag ("WeaponPanel");
		weapons = weaponPanel.GetComponentsInChildren(typeof(Image), true);
		foreach (Image item in weapons) {
			item.gameObject.SetActive (true);
			weaponlist.Add (item);
		}
		weaponlist.RemoveAt(0); // unnecessary image of panel not a weapon
		InitBulletsCounters();
        //for (int i = 0; i < weaponlist.Count; i++) 
        //{
        //	bulletstext = weaponlist [i].GetComponentInChildren<Text> ();
        //	bulletstext.text = bulletscounter.ToString ();
        //}
        bulletstext = weaponlist[0].GetComponentInChildren<Text>();
        bulletstext.text = bulletscounter.ToString ();
        //SelectWeapon (selectedweapon);
        //		foreach (var item in bullets) {
        //			item.gameObject.SetActive (true);
        //			var oldcolor = item.color;
        //			Color newcolor = new Color (oldcolor.r, oldcolor.g, oldcolor.b, 100f);
        //		}
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown ("Fire2")  ) 
		{
			FireAttack ();
        }
	//	int previousSelectedWeapon = selectedweapon;
	//	if (Input.GetAxis ("Mouse ScrollWheel") > 0f) 
	//	{
	//		if (selectedweapon >= weaponlist.Count - 1) 
	//		{
	//			selectedweapon = 0;
	//		} 
	//		else 
	//		{
	//			selectedweapon++;
	//		}
	//	}
	//	if (Input.GetAxis ("Mouse ScrollWheel") < 0f) 
	//	{
	//		if (selectedweapon <= 0) 
	//		{
	//			selectedweapon = weaponlist.Count-1;
	//		} 
	//		else 
	//		{
	//			selectedweapon--;
	//		}
	//	}
	//	if (previousSelectedWeapon != selectedweapon) 
	//	{
	//		SelectWeapon (selectedweapon);
	//	}
	}



	//private void SelectWeapon(int number)
	//{
	//	Image currentitem;
	//	currentitem = weaponlist [number];
	//	var oldcolor = weaponlist [number].color;
	//	Color newcolor = new Color (oldcolor.r, oldcolor.g, oldcolor.b, 1f);
	//	weaponlist [number].color = newcolor;
	//	foreach (var item in weaponlist) {
	//		if (item != currentitem) {
	//			var oldcoloritems = item.color;
	//			Color newitemscolor = new Color (oldcoloritems.r, oldcoloritems.g, oldcoloritems.b, 0.2f);
	//			item.color = newitemscolor;
	//		}
	//	}
	//}

	private void InitBulletsCounters ()
	{
		for (int i = 0; i < 1; i++) {
			bulletscounter=10;
		}
	}

	private void FireAttack()
	{
        isRight = MySingleton.Instance().direction/*LaraCroftMoving.isFacingRight*/; /*GetComponent<LaraCroftMoving> ().isFacingRight;*/
        MakeShot(isRight);
		//if(isRight)
		//{
		//	if (bulletscounter> 0) {
  //              FindObjectOfType<AudioManager>().Play("Knife");
  //              GameObject bullcurrent;
  //              bullcurrent = Instantiate(bullet[0], new Vector3(transform.position.x + 0.3f, transform.position.y, transform.position.z), Quaternion.identity, bulletcontainer.transform);
  //              bullcurrent.GetComponent<BulletScript>().SetRotation(isRight);
  //              bulletscounter--;
  //              UpdateBulletsText();
  //              //            bullcurrent = Instantiate (bullet [selectedweapon], new Vector3 (transform.position.x + 0.3f, transform.position.y, transform.position.z), Quaternion.identity, bulletcontainer.transform);
  //              //bullcurrent.GetComponent<BulletScript> ().isRight = isRight;
  //              //bulletscounter [selectedweapon]--;
  //              //UpdateBulletsText (selectedweapon);
  //          }
		//}
		//else 
		//{
		//	if (bulletscounter> 0) {
  //              FindObjectOfType<AudioManager>().Play("Knife");
  //              GameObject bullcurrent;
  //              bullcurrent = Instantiate(bullet[0], new Vector3(transform.position.x - 0.3f, transform.position.y, transform.position.z), Quaternion.identity, bulletcontainer.transform);
  //              bullcurrent.GetComponent<BulletScript>().SetRotation(isRight);
  //              bulletscounter--;
  //              UpdateBulletsText();
  //              //            bullcurrent = Instantiate (bullet [selectedweapon], new Vector3 (transform.position.x - 0.3f, transform.position.y, transform.position.z), Quaternion.identity, bulletcontainer.transform);
  //              //bullcurrent.GetComponent<BulletScript> ().isRight = isRight;
  //              //bulletscounter [selectedweapon]--;
  //              //UpdateBulletsText (selectedweapon);
  //          }
		//}
	}

    //private void UpdateBulletsText (int selectedweapon)
    //{
    //	bulletstext [selectedweapon].text = bulletscounter [selectedweapon].ToString ();
    //}
    private void UpdateBulletsText()
    {
        bulletstext.text = bulletscounter.ToString();
    }
    private void MakeShot(bool direction)
    {
        if (bulletscounter > 0)
        {
            FindObjectOfType<AudioManager>().Play("Knife");
            GameObject bullcurrent;
            if(direction)
            {
                bullcurrent = Instantiate(bullet[0], new Vector3(transform.position.x + 0.3f, transform.position.y, transform.position.z), Quaternion.identity, bulletcontainer.transform);
            }
            else
            {
                bullcurrent = Instantiate(bullet[0], new Vector3(transform.position.x - 0.3f, transform.position.y, transform.position.z), Quaternion.identity, bulletcontainer.transform);
            }
            bullcurrent.GetComponent<BulletScript>().SetRotation(direction);
            bulletscounter--;
            UpdateBulletsText();
        }
        else
        {
            return;
        }
    }
}