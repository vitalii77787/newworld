﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArtefactPlace : MonoBehaviour {
    [SerializeField]
    private GameObject infopanel;
    private Text panelText;
    private bool isActive;
    private bool flag;
    [SerializeField]
    private int artefactnumber;
    [SerializeField]
    private bool hasArtefact;
    string TextToDisplay;
    // Use this for initialization
    void Start () {
        isActive = false;
        flag = true;
        panelText = infopanel.GetComponentInChildren<Text>();
       
	}

	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1") && isActive && flag && hasArtefact)
        {
            GameManager.Instance().SetArtefact(artefactnumber);
            isActive = false;
            infopanel.SetActive(false);
            flag = false;
            hasArtefact = false;
        }
    }
    void OnTriggerEnter2D(Collider2D сollision)
    {
        if (сollision.gameObject.tag == "Player" && flag && hasArtefact )
        {
            isActive = true;
            infopanel.SetActive(true);
            panelText.text = TextToDisplay;
        }
    }
    void OnTriggerExit2D(Collider2D сollision)
    {
        if (сollision.gameObject.tag == "Player" && flag && hasArtefact)
        {
            isActive = false;
            infopanel.SetActive(false);
            panelText.text = " ";
        }
    }
    public void InitArtefact(int number, bool isartefact)
    {
        artefactnumber = number;
        hasArtefact = isartefact;
        TextToDisplay = "Congratulations, you find the " + ++number + " part of artefact!";
    }  
}
