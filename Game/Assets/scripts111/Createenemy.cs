﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Createenemy : MonoBehaviour {
	float timer;
	public GameObject pref;
	// Use this for initialization
	void Start () {
		timer = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player" && timer > 10) 
		{
			timer = 0;
			Instantiate (pref, new Vector3 (transform.position.x + 2, transform.position.y, transform.position.z), Quaternion.identity);
		}
	}
}
