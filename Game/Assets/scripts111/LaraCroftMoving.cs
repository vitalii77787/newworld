﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaraCroftMoving : MonoBehaviour {
    public float maxSpeed = 10f;
    //public Transform sensGround;
    //public LayerMask layerGround;
    //переменная для определения направления персонажа вправо/влево
   // public static bool isFacingRight = true;
    //ссылка на компонент анимаций
    private Animator anim;
    Rigidbody2D pers;
    public bool isGround = false;
    public Vector2 jump;
    SpriteRenderer spr;
    public Par paral;
    Transform cam;
    Vector3 deltaCam;
    float posX;
    float posY;
    public float move;
    // Use this for initialization
    void Start()
    {
        move = 0;
        spr = GetComponent<SpriteRenderer>();
        //sensGround = GetComponentInChildren<Transform> ();
        anim = GetComponent<Animator>();
        pers = GetComponent<Rigidbody2D>();
        jump = new Vector2(0, 180);
        cam = Camera.main.transform;
        deltaCam = cam.position - transform.position;
        posX = transform.position.x;
        posY = transform.position.y;
    }

    void Update()
    {
        SetParallax();
        HorizontalMove();
        if (!isGround)
        {
            maxSpeed = 2.5f;
        }
        if (isGround)
        {
            maxSpeed = 5f;
        }
        if (isGround && Input.GetKeyDown(KeyCode.Space) && !MySingleton.Instance().isattacking)
        {
            pers.AddForce(jump);
            //isGround = false;
        }
    }
    private void FixedUpdate()
    {
        isGround = Physics2D.Raycast(transform.position, Vector2.down, 0.3f);
    }


    private void Flip()
    {
        MySingleton.Instance().direction = !MySingleton.Instance().direction;
        spr.flipX = !spr.flipX;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.tag == "ground")
        {
            isGround = true;
        }
    }
    private void SetParallax()
    {
        cam.position = transform.position + deltaCam;
        paral.run = (transform.position.x - posX) * 20f;
        posX = transform.position.x;
        paral.jump = (transform.position.y - posY);
        posY = transform.position.y;
    }
    private void HorizontalMove()
    {
        anim.SetBool("isGround", isGround);
        anim.SetFloat("ySpeed", pers.velocity.y);
        if (!MySingleton.Instance().isattacking)
        {
            move = Input.GetAxis("Horizontal");
        }
        anim.SetFloat("xSpeed", Mathf.Abs(move));
        pers.velocity = new Vector2(move * maxSpeed, pers.velocity.y);
        if (move > 0 && !MySingleton.Instance().direction)
            Flip();
        else if (move < 0 && MySingleton.Instance().direction)
            Flip();
    }  
   
}
