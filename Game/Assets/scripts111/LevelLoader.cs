﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LevelLoader : MonoBehaviour {
    public GameObject loadinScreen;
    public Slider slider;
   
	// Use this for initialization
	void Start () {
		
	}
	
	public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronusly(sceneIndex));
    }

    private IEnumerator LoadAsynchronusly(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loadinScreen.SetActive(true);
        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value =operation.progress;
            yield return null;
        }
    }
}
