﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterControllerScript : MonoBehaviour {
    public Text bulletscount;
    GameObject weaponImage;
    public float maxSpeed = 10f; 
	//public Transform sensGround;
	public LayerMask layerGround;
	//переменная для определения направления персонажа вправо/влево
	public bool isFacingRight = true;
	//ссылка на компонент анимаций
	private Animator anim;
	Rigidbody2D pers;
	bool isGround=false;
	public Vector2 jump;
	SpriteRenderer spr;

	public Par paral;
	Transform cam;
	Vector3 deltaCam;
	float posX;
	float posY;
    public GameObject currentWeapon;
    const float offset = 0.05f;
    public GameObject bullet;
    public int pistolBulletCount;

    // Use this for initialization
    void Start () {
        weaponImage = GameObject.FindGameObjectWithTag("weaponImage");
        pistolBulletCount = 12;
        bulletscount.text = pistolBulletCount.ToString();
		spr = GetComponent<SpriteRenderer> ();
		//sensGround = GetComponentInChildren<Transform> ();
		anim = GetComponent<Animator>();
		pers = GetComponent<Rigidbody2D>();
		jump = new Vector2 (0, 180);

		cam = Camera.main.transform;
		deltaCam = cam.position - transform.position;
		posX = transform.position.x;
		posY = transform.position.y;
        currentWeapon = GameObject.FindGameObjectWithTag("pistol");
        currentWeapon.GetComponent<Renderer>().enabled = false;
        currentWeapon = GameObject.FindGameObjectWithTag("machinegun");
        currentWeapon.GetComponent<Renderer>().enabled = false;
    }

	void Update () {

		cam.position = transform.position + deltaCam;
		paral.run = (transform.position.x - posX) * 20f;
		posX = transform.position.x;
		paral.jump = (transform.position.y - posY);
		posY = transform.position.y;

		if (isGround && Input.GetKeyDown (KeyCode.Space)) {
			pers.AddForce (jump);
			isGround = false;
		}
		if (Input.GetButtonDown("Fire1")) {
			anim.Play("Attack");
		}
        if(Input.GetButtonDown("Fire2"))
        {
            FireBullet(spr);
        }
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
         
            ChooseWeapon("pistol");
            
        }
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            ChooseWeapon("machinegun");
            weaponImage.GetComponent<SpriteRenderer>().sprite = GameObject.FindGameObjectWithTag("machinegun").GetComponent<SpriteRenderer>().sprite;
        }
		isGround = Physics2D.Raycast (transform.position, Vector2.down, 0.5f,layerGround);
	}

    private void FireBullet(SpriteRenderer spr)
    {
        if (pistolBulletCount > 0)
        {
            if (spr.flipX)
            {
                FireBulletWithDirection(0.25f, false);
                
                
            }
            else
            {
                FireBulletWithDirection(-0.2f, true);
            }
            pistolBulletCount--;
            bulletscount.text = pistolBulletCount.ToString();
        }
    }

    private void FireBulletWithDirection(float xOffset, bool direction)
    {
        GameObject currentBullet = Instantiate(bullet,
                                               new Vector3(transform.position.x - xOffset, transform.position.y - 0.1f, transform.position.z),
                                               Quaternion.identity);
        currentBullet.GetComponent<BulletScript>().isRight = direction;
    }

    private void ChooseWeapon(string weaponTag)
    {
        currentWeapon.GetComponent<Renderer>().enabled = false;
        currentWeapon = GameObject.FindGameObjectWithTag(weaponTag);
        currentWeapon.GetComponent<Renderer>().enabled = true;
    }

	private void FixedUpdate()
	{
		print (isGround);
		anim.SetBool("isGround", isGround);
		anim.SetFloat("ySpeed", pers.velocity.y);
		float move = Input.GetAxis("Horizontal");
		anim.SetFloat("xSpeed", Mathf.Abs(move));
		pers.velocity = new Vector2(move * maxSpeed, pers.velocity.y);
		if(move > 0 && !isFacingRight)
			Flip();
		else if (move < 0 && isFacingRight)
			Flip();
	}


	private void Flip()
	{
		isFacingRight = !isFacingRight;
		spr.flipX=!spr.flipX;
        if(isFacingRight)
        {
            currentWeapon.GetComponent<SpriteRenderer>().flipX = spr.flipX;
            currentWeapon.transform.position = new Vector3(transform.position.x + offset, transform.position.y - 0.1f, 0);
        }
      else
        {
            currentWeapon.GetComponent<SpriteRenderer>().flipX = spr.flipX;
            currentWeapon.transform.position = new Vector3(transform.position.x - offset, transform.position.y - 0.1f, 0);
        }
    }

	void OnCollisionEnter(Collision col)
	{
		print("Collision");
		if (col.transform.tag== "ground") 
		{
			isGround = true;
		}
	}
}