﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healthpack : MonoBehaviour {
	PlayerHealth playerHealth;
	GameObject player; 
	AudioSource audioclip;
	bool istoken;
	// Use this for initialization
	void Start () {
		audioclip = GetComponent<AudioSource> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
	}
	


	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject==player && !istoken) 
		{
			istoken = true;
			audioclip.Play ();
			GetComponent<SpriteRenderer> ().enabled=false;
			playerHealth.TakeHealth (20);
			Destroy (gameObject,1f);
		}
	}
}
