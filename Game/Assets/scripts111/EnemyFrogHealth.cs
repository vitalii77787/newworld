﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemyFrogHealth : MonoBehaviour {

    [SerializeField]
    protected GameObject hitMarkerPrefab;
    [SerializeField]
    protected float markerKillTime;
    public GameObject gold;
    public Image healthBar;
    public float maxHealth = 100;                            // The amount of health the player starts the game with.
    public float currentHealth;
    protected EnemyFrogMooving enemymove;
    protected EnemyFrogAttack attack;
    //public Scrollbar healthbar;  
    protected Animator anim;
    protected bool isDead;
    protected  GameObject player;
    protected Collider2D enemycollider;
    protected Rigidbody2D enemyrigidbody;
    // Use this for initialization
   protected void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        attack = GetComponent<EnemyFrogAttack>();
        currentHealth = maxHealth;
        anim = GetComponent<Animator>();
        enemymove = GetComponent<EnemyFrogMooving>();
        enemycollider = GetComponent<Collider2D>();
        enemyrigidbody = GetComponent<Rigidbody2D>();
        UpdateHealthBar();

    }

    // Update is called once per frame
    void Update()
    {
    }
    public void TakeDamage(int amount)
    {
        WithdrawHealth(amount);
        UpdateHealthBar();
        // Play the hurt sound effect.
        //playerAudio.Play ();
    }
    protected virtual void Death()
    {
        // Set the death flag so this function won't be called again.
        isDead = true;
        // Turn off any remaining shooting effects.
        //playerShooting.DisableEffects ();
        // Tell the animator that the player is dead.
        anim.Play("FrogDeath");
        // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
        //playerAudio.clip = deathClip;
        //playerAudio.Play ();
        // Turn off the movement and shooting scripts.
        DisableAttack();
        DisableMovement();
        CreateGold();
        Destroy(enemycollider, 0.4f);
        Destroy(gameObject, 1f);
        //playerShooting.enabled = false;
    }
    private void UpdateHealthBar()
    {
        healthBar.fillAmount = GetHealthValue();
    }

    private float GetHealthValue()
    {
        return (float)currentHealth / (float)maxHealth;
    }

    private void WithdrawHealth(int amount)
    {
        if (currentHealth - amount <= 0 && !isDead)
        {
            CreateHitMarker(amount);
            currentHealth = 0;
            // If the player has lost all it's health and the death flag hasn't been set yet...
            Death();
        }
        else
        {
            currentHealth -= amount;
            CreateHitMarker(amount);
        }
    }

    protected void CreateGold()
    {
            gold = Instantiate(gold, new Vector2(transform.position.x, transform.position.y + 0.3f), transform.rotation);
            gold.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-150f, 150f), 800));
    }
    protected void DisableMovement()
    {
        enemymove.enabled = false;
    }
    protected void DisableAttack()
    {
        attack.enabled = false;
    }
    protected void CreateHitMarker(float damageamount)
    {
        Color markercolor = Color.green;
        if(currentHealth<(0.66f*maxHealth))
        {
            markercolor = Color.yellow;
        }
        if (currentHealth < (0.33f * maxHealth))
        {
            markercolor = Color.red;
        }
        GameObject newMarker = Instantiate(hitMarkerPrefab, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), Quaternion.identity);
        newMarker.GetComponent<DamageMarkerController>().SetTextAndMove(damageamount.ToString(), markercolor);
        Destroy(newMarker.gameObject, markerKillTime);
    }
}
