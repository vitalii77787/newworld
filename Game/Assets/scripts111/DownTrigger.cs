﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") {
			col.transform.position = new Vector2 (-10f, -4f);
		} else 
		{
			Destroy (col.gameObject);
		}
	}
}
